use std::fs;
use std::net::{TcpListener, TcpStream};
use std::io::{BufReader, BufRead, Write};

fn manage_index(request: Vec<String>) -> String {
    fs::read_to_string("page/index.html").unwrap()
}

fn manage_login(request: Vec<String>) -> String {
    fs::read_to_string("page/login.html").unwrap()
}

fn manage_products(request: Vec<String>) -> String {
    fs::read_to_string("page/products.html").unwrap()
}

fn manage_credits(request: Vec<String>) -> String {
    fs::read_to_string("page/credits.html").unwrap()
}

fn manage_connexion(mut stream: TcpStream) {
    let buf_reader = BufReader::new(&mut stream);
    let request: Vec<_> = buf_reader
        .lines()
        .map(|result| result.unwrap())
        .take_while(|line| !line.is_empty())
        .collect();

    let header : Vec<_> = request[0].split(" ").collect();
    if header[0] == "GET" {
        let pages = [
            (String::from("/"), manage_index as fn(Vec<String>) -> String),
            (String::from("/login"), manage_login as fn(Vec<String>) -> String),
            (String::from("/produits"), manage_products as fn(Vec<String>) -> String),
            (String::from("/credits"), manage_credits as fn(Vec<String>) -> String),
        ];
        let mut content = String::new();
        for (page, handle) in pages {
            if page == header[1] {
                content = handle(request.clone());
                break;
            }
        }

        if !content.is_empty() {
            let status = "HTTP/1.1 200 OK";
            let length = content.len();

            let responce = format!("{status}\r\nContent-Length: {length}\r\n\r\n{content}");
            stream.write_all(responce.as_bytes()).unwrap();
        } else {
            let status = "HTTP/1.1 404 NOT FOUND";
            let content = fs::read_to_string("page/404.html").unwrap();
            let length = content.len();

            let responce = format!("{status}\r\nContent-Length: {length}\r\n\r\n{content}");
            stream.write_all(responce.as_bytes()).unwrap();
        }
    }
    println!("Request:\n {:#?}", request);
}

fn main() {
    let listener = match TcpListener::bind("0.0.0.0:2048") {
        Ok(lst) => lst,
        Err(err) => panic!("Error: {}", err),
    };

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        manage_connexion(stream);
    }
}
